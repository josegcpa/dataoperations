# Data operations to handle MRI scans and train deep learning models

This repository contains `python` scripts that handle data from MRI scans and converts them into a PyTorch-convenient format.

## Contents

`lib/data_functions.py` contains all of the necessary classes (`TorchIODataset`, `NumpyDataset` and `MONAIDataset`) for data loading using different packages:

* `TorchIODataset` - uses the [TorchIO](https://torchio.readthedocs.io/) package for loading and processing MRI images 
* `NumpyDataset` - my own implementation (intermediate operations such as padding are all performed in `numpy`, hence the name) of a Torch `Dataset` for `.dicom` and  `.nii` files (can only handle these two)
* `MONAIDataset` - uses the [MONAI](https://docs.monai.io/en/stable/) package for loading and processing MRI images. So far my favourite - fast and returns Tensors, making its integration with other pipelines much more convenient

All of them work with `path_dictionary` (details below). In `data-generator-benchmark.ipynb` I have included a way to benchmark these Dataset classes. The main conclusions are (possibly fairly obvious ones but I will state them for completeness sake):

* NIFTI is a considerably more efficient format than DICOM
* TorchIO is not as efficient as MONAI when handling NIFTI files (but is comparable for DICOM format scans)
* Padding operations are fairly time consuming so should be done *before* training to avoid large I/O bottlenecks

## `path_dictionary` - a convenience format

The `path_dictionary` format can be simply described in the following way:

```
path_dictionary
    |-MRI Scan 1
    | |-Modality 1: path_a
    | |-Modality 2: path_b
    |-MRI Scan 2:
    |-Modality 1: path_c
    |-Modality 2: path_d
```

In other words, the `path_dictionary` is an dictionary of dictionaries - the parent dictionary is indexed using arbitrary MRI scan identifiers (use whatever you want) and stores as values child dictionaries, indexed with modality identifiers and storing paths to MRI scans. So, for instance, if I have my `path_dictionary` and want want to access the `T1_weighted` scan of `MRI 42`, I would have to call `path_dictionary["MRI 42"]["T1_weighted"]`.