import os
import numpy as np
import pydicom
import SimpleITK as sitk
import time
from tqdm import tqdm
from glob import glob

# code to resample a dataset with varying spacings to get them all to 
# have the same spacing between them

def resample_volume(volume,new_spacing):
    # based partly on Joao Santinha's workshop and a few other internet 
    # threads
    volume = sitk.Cast(volume, sitk.sitkFloat32)

    orig_size = np.array(volume.GetSize(), dtype=np.int)
    orig_spacing = volume.GetSpacing()
    new_size = orig_size * np.divide(orig_spacing, new_spacing)
    new_size = np.ceil(new_size).astype(np.int)
    new_size = [int(s) for s in new_size]

    resampled_volume = sitk.Resample(
        volume,new_size, sitk.Transform(),sitk.sitkLinear, 
        volume.GetOrigin(),new_spacing,volume.GetDirection())
    return resampled_volume

def mkdir(x):
    try: os.makedirs(x)
    except: pass

all_dcm_folders = glob('../../data/PROSTATE-DIAGNOSIS/PROSTATE-DIAGNOSIS/*/*')

K = ["T1WTSEAX","T2WTSEAX","T2WTSECOR"]
path_dictionary = {}

for folder in all_dcm_folders:
    folder_name = folder.split(os.sep)[-2]
    path_dictionary[folder_name] = {}
    for f in glob("{}/*".format(folder)):
        for k in K:
            if k in f.split('-')[-2]:
                path_dictionary[folder_name][k] = f

all_mod_spacings = {}
i = 0
for path in path_dictionary:
    mod_paths = path_dictionary[path]
    for mod_k in mod_paths:
        mod_path = mod_paths[mod_k]
        dcm_paths = glob('{}/*dcm'.format(mod_path))
        random_dcm_path = np.random.choice(dcm_paths)
        f = pydicom.dcmread(random_dcm_path)
        slice_thickness = np.asarray([f[("0018", "0088")].value])
        spacing = np.asarray(f[("0028","0030")].value)
        spacing = np.concatenate([spacing,slice_thickness])
        if mod_k not in all_mod_spacings:
            all_mod_spacings[mod_k] = [spacing]
        else:
            all_mod_spacings[mod_k].append(spacing)

maximum_spacings = {}
for k in all_mod_spacings:
    maximum_spacings[k] = np.array(all_mod_spacings[k]).max(axis=0)

output_dir = '../../data/PROSTATE-DIAGNOSIS/PROSTATE-DIAGNOSIS-RESIZED/'

for path in tqdm(path_dictionary):
    mkdir('{}/{}'.format(output_dir,path))
    mod_paths = path_dictionary[path]
    for mod_k in mod_paths:
        mkdir('{}/{}/{}'.format(output_dir,path,mod_k))
        reader = sitk.ImageSeriesReader()
        reader.MetaDataDictionaryArrayUpdateOn()
        reader.LoadPrivateTagsOn()
        
        mod_path = mod_paths[mod_k]
        dcm_paths = glob('{}/*dcm'.format(mod_path))
        random_dcm_path = np.random.choice(dcm_paths)
        f = pydicom.dcmread(random_dcm_path)
        slice_thickness = np.asarray([f[("0018", "0088")].value])
        spacing = np.asarray(f[("0028","0030")].value)
        spacing = np.concatenate([spacing,slice_thickness])
        dicom_names = reader.GetGDCMSeriesFileNames(mod_path)
        reader.SetFileNames(dicom_names)
        image = reader.Execute()
        if np.all(spacing == maximum_spacings[mod_k]) == True:
            pass
        else:
            image = resample_volume(image,maximum_spacings[mod_k])

        writer = sitk.ImageFileWriter()
        writer.KeepOriginalImageUIDOn()
        good_i = 0
        st = maximum_spacings[mod_k][-1]
        for i in range(image.GetDepth()):
            sl = image[:,:,i:(i+1)]
            failure = False
            try:
                for k in reader.GetMetaDataKeys(0):
                    v = reader.GetMetaData(i,k)
                    if v != "":
                        good_i = i
                        sl.SetMetaData(k,v)

                sl.SetMetaData("2001|100a",str(i+1)) # manually setting slice number
                t = str(slice_thickness*i)
                sl.SetMetaData("0018|0050",str(st)) # manually setting slice thickness
                sl.SetMetaData("0020|1041",t) # manually setting slice position
                
                sl.SetDirection(image.GetDirection())
                sl.SetOrigin(image.GetOrigin())
                sl.SetSpacing(image.GetSpacing())
                i_str = (3-len(str(i))) * '0' + str(i+1)
                output_path = '{}/{}/{}/{}.dcm'.format(output_dir,path,mod_k,i_str)
                writer.SetFileName(output_path)
                writer.Execute(sl)
            except:
                pass