input_dir=../../data/PROSTATE-DIAGNOSIS/PROSTATE-DIAGNOSIS-RESIZED
output_dir=../../data/PROSTATE-DIAGNOSIS/PROSTATE-DIAGNOSIS-RESIZED-NIFTI

mkdir -p $output_dir

for folder in $input_dir/*
do
    root=$(basename $folder)
    o=$output_dir/$root
    mkdir -p $o
    dcm2niix -o $o $folder
done