import os
import numpy as np
import pydicom
import itk
import SimpleITK as sitk
import torch
import torchvision
import nibabel
import torchio
import monai
from scipy.stats import norm
from pydicom.filereader import read_dicomdir
from glob import glob
from copy import deepcopy
from math import floor
from enum import Enum
from typing import Dict,List,Union,Collection,Hashable,Sequence,Mapping

PathDictionary = dict[dict[str]]
TensorList = List[torch.Tensor]

def orientation_ras_lps(affine: np.array) -> np.array:
    """Converts RAS orientation to LPS. Adapted from MONAI

    Args:
        affine (np.array): affine matrix in RAS coordinates

    Returns:
        affine (np.array) : affine matrix in LPS coordinates
    """
    sr = max(affine.shape[0] - 1, 1)  # spatial rank is at least 1
    flip_d = [[-1, 1], [-1, -1, 1], [-1, -1, 1, 1]]
    flip_diag = flip_d[min(sr - 1, 2)] + [1] * (sr - 3)
    if isinstance(affine, torch.Tensor):
        return torch.diag(torch.as_tensor(flip_diag).to(affine)) @ affine  # type: ignore
    return np.diag(flip_diag).astype(affine.dtype) @ affine  # type: ignore

def get_affine_pydicom(f: pydicom.FileDataset,
                       lps_to_ras: bool=True) -> dict:
    """Calculates the affine matrix for pydicom. 
    Adapted from the MONAI equivalent for ITKReader

    Args:
        f (pydicom FileDataset): an object resulting from `pydicom.dcmread`
        lps_to_ras (bool, optional): Whether coordianates should be converted from
        LPS to RAS. Defaults to True.

    Returns:
        output_dict (dict): a dictionary containing the `direction`, `spacing`,
        `origin` and `affine` as returned by ITKReader in MONAI
    """
    slice_thickness = np.asarray([f[("0018", "0088")].value])
    direction = np.asarray(f[("0020","0037")].value)
    direction = np.reshape(direction,[-1,3])
    direction = np.concatenate([
        direction,
        np.cross(direction[0,:],direction[1,:])[np.newaxis,:]]).T
    spacing = np.asarray(f[("0028","0030")].value)
    spacing = np.concatenate([spacing,slice_thickness])
    origin = np.asarray(f[("0020", "0032")].value)

    direction = np.asarray(direction)
    sr = min(max(direction.shape[0], 1), 3)
    affine: np.ndarray = np.eye(sr + 1)
    affine[:sr, :sr] = direction[:sr, :sr] @ np.diag(spacing[:sr])
    affine[:sr, -1] = origin[:sr]
    if lps_to_ras:
        affine = orientation_ras_lps(affine)
    
    output_dict = {'direction':direction,'spacing':spacing,
                   'origin':origin,'affine':affine}
    
    return output_dict

def read_pydicom(folder: str) -> np.array:
    """Reads a folder of dcm files and returns an array + metadata

    Args:
        folder (str): path to folder containing .dcm files

    Returns:
        stacked_image (np.array): MRI scan array for a specific modality
        metadata (dict): dictionary containing direction, spacing, origin,
        affine, spatial_shape and original_affine (similar to the one 
        returned by ITKReader in MONAI)
    """
    files = read_dicomdir(folder)
    metadata = get_affine_pydicom(files[-1])
    files = sorted(files,key = lambda x: x.SliceLocation)
    stacked_image = np.stack([x.pixel_array for x in files])
    stacked_image = stacked_image.astype(np.float32)
    rs = f.RescaleSlope
    stacked_image = stacked_image/rs
    metadata['spatial_shape'] = np.asarray(stacked_image.shape)
    metadata['original_affine'] = metadata['affine'].copy()
    return stacked_image,metadata

def itk_reader(path: str) -> np.array:
    """Convenience function to read DICOM folder and return `np.array`

    Args:
        path (str): path to folder containing DICOM files

    Returns:
        (np.array): `np.array` for the MRI scan in `path`
    """
    return itk.GetArrayFromImage(itk.imread(path))

def nifti_reader(path: str) -> np.array:
    """Convenience function to read NIfTi and return `np.array`

    Args:
        path (str): path to NIfTi file

    Returns:
        (np.array): `np.array` for the MRI scan in `path`
    """
    nib_obj = nibabel.load(path)
    X = nib_obj.get_fdata()
    return np.asarray(X)

def make_one_hot(i,l):
    one_hot = np.zeros(l,dtype=np.float32)
    one_hot[i] = 1
    return one_hot

class NumpyDataset(torch.utils.data.Dataset):
    def __init__(self,path_dictionary: PathDictionary,
                 data_type: str,
                 padding: bool=False,
                 transforms: List=None,
                 to_tensor: bool=False) -> torch.utils.data.Dataset:
        """Joint dataset class for the generation of data from 
        a `path_dictionary` (details below). Works with numpy arrays

        Args:
            path_dictionary (dict): a dictionary containing paths
            to MRI scans.
            data_type: either "dicom" or "nifti"
            padding (bool,optional): pads the data to the inferred maximum 
            size. Defaults to False
            transforms (dict, optional): dict containing lists of PyTorch transforms,
            one for each key. Defaults to None
            to_tensor (bool, optional): whether the output should be converted
            to a Pytorch tensor. Defaults to False
            
        More particularly, the `path_dictionary` is structured as a
        dictionary where each key is an "MRI scan key" and the value 
        is a dictionary containing one path (folder) for each modality key. 
        In other words:
            
            path_dictionary
             |-MRI Scan 1
             | |-Modality 1: path_a
             | |-Modality 2: path_b
             |-MRI Scan 2:
               |-Modality 1: path_c
               |-Modality 2: path_d
               
        Or, more pythonically:
        `path_dictionary = {
            "MRI Scan 1": {"Modality 1": "path_a","Modality 2": "path_b"},
            "MRI Scan 2": {"Modality 1": "path_c","Modality 2": "path_d"}}`
        """
        
        self.path_dictionary = path_dictionary
        self.data_type = data_type
        self.padding = padding
        self.transforms = transforms
        if self.transforms is None:
            self.transforms = {}
        self.to_tensor = to_tensor
        self.keys = list(path_dictionary.keys())
        self.keys_original = self.keys.copy()
        self.length = len(self.keys)

        self.infer_mod_keys()
        self.automatic_id_exclusion()
        self.setup_transform_ops()
        if self.data_type == "dicom":
            self.infer_max_dimensions_dicom()
            self.image_loader = itk_reader
        else:
            self.infer_max_dimensions_nifti()
            self.image_loader = nifti_reader

    def __len__(self):
        """Returns the number of MRI scans.

        Returns:
            length : Number of MRI scans
        """
        return self.length

    def __getitem__(self, index):
        """Returns the `self.keys[index]` MRI scan.

        Args:
            index (int): index of the MRI scan
            padding (bool, optional): pads each modality according to 
            `self.max_shapes`. Defaults to False.

        Returns:
            output_dict (dict): Dictionary containing `np.array` (value)
            for each modality (key)
        """
        if isinstance(index,str):
            k = index
        else:
            k = self.keys[index]

        output_dict = {}
        for mod_k in self.mod_keys:
            path = self.path_dictionary[k][mod_k]
            X = self.image_loader(path)
            if self.padding == True:
                X = self.pad(X,self.max_shapes[mod_k])
            X = np.expand_dims(X,0)
            if self.to_tensor == True:
                X = torch.Tensor(X)
                if self.transform_ops[mod_k] is not None:
                    X = self.transform_ops[mod_k](X)
            output_dict[mod_k] = X
        return output_dict
    
    def setup_transform_ops(self):
        self.transform_ops = {}
        for mod_k in self.mod_keys:
            if mod_k in self.transforms:
                self.transform_ops[mod_k] = torchvision.transforms.Compose(
                    self.transforms[mod_k])
            else:
                self.transform_ops[mod_k] = None
                
    def update_keys(self,new_keys):
        """Updates MRI scan keys quickly. Allows the quick
        exchange between training/testing keys.

        Args:
            new_keys (list): list of keys (they need to be
            a subset of the original keys)
        """
        self.keys = []
        for k in new_keys:
            if k in self.keys_original:
                self.keys.append(k)
        self.length = len(self.keys)
    
    def infer_mod_keys(self):
        """Infers the keys for different modalities. If a modality
        is not present in at least 60% of all cases it gets excluded.
        """
        all_mod_keys = {}
        self.mod_keys = []
        self.excluded_mod_keys = []
        for k in self.keys:
            for kk in self.path_dictionary[k]:
                if kk not in all_mod_keys:
                    all_mod_keys[kk] = 1
                else:
                    all_mod_keys[kk] += 1
        for k in all_mod_keys:
            if (all_mod_keys[k] / self.length) > 0.6:
                self.mod_keys.append(k)
            else:
                self.excluded_mod_keys.append(k)
        self.n_modalities = len(self.mod_keys)
                
    def automatic_id_exclusion(self):
        """Excludes MRI scan keys based on modality key completeness.
        MRI scan keys are kept iif they have all the necessary 
        modalities.
        """
        good_keys = []
        bad_keys = []
        for k in self.keys:
            curr_keys = list(self.path_dictionary[k].keys())
            good = True
            for kk in self.mod_keys:
                if kk not in curr_keys:
                    good = False
            if good == True:
                good_keys.append(k)
            else:
                bad_keys.append(k)
        self.keys = good_keys
        self.excluded_keys = bad_keys
        self.length = len(self.keys)

    def get_random_images(self,n=1):
        """Generates a random set images from each MRI scan and 
        each modality.

        Args:
            n (int, optional): Number of images to be generated 
            for each MRI scan and modality. Defaults to 1.

        Yields:
            (pydicom array, mod_key): (a pydicom array, the 
            modality key)
        """
        for k in self.keys:
            for mod_key in self.mod_keys:
                mod_path = self.path_dictionary[k][mod_key]        
                all_dcm_paths = glob('{}/*dcm'.format(mod_path))
                random_dcm_paths = np.random.choice(all_dcm_paths,size=n)
                for dcm_path in random_dcm_paths:
                    yield pydicom.dcmread(dcm_path),mod_key
    
    def infer_max_dimensions_dicom(self):
        """Infers the maximum dimensions. Used for padding.
        """
        self.max_shapes = {}
        # get maximum height + width
        shapes = {mod_key:[] for mod_key in self.mod_keys}
        for dcm_image,mod_key in self.get_random_images():
            shapes[mod_key].append(dcm_image.pixel_array.shape)
        for mod_key in self.mod_keys:
            self.max_shapes[mod_key] = np.array(
                shapes[mod_key]).max(axis=0).tolist()
        
        # get maximum number of slices, combine with height + width
        n_slices = {mod_key:[] for mod_key in self.mod_keys}
        for k in self.keys:
            for mod_key in self.mod_keys:
                mod_path = self.path_dictionary[k][mod_key]
                n_slices[mod_key].append(len(glob('{}/*dcm'.format(mod_path))))
        for mod_key in n_slices:
            max_n_slices = np.max(n_slices[mod_key])
            self.max_shapes[mod_key].append(max_n_slices)
            self.max_shapes[mod_key].reverse() # channels first

    def infer_max_dimensions_nifti(self):
        """Infers the maximum dimensions. Used for padding.
        """
        self.max_shapes = {}
        # get maximum height + width
        shapes = {mod_key:[] for mod_key in self.mod_keys}
        for k in self.keys:
            for mod_key in self.mod_keys:
                path = self.path_dictionary[k][mod_key]
                image = nibabel.load(path)
                shapes[mod_key].append(image.header.get_data_shape())

        for mod_key in shapes:
            self.max_shapes[mod_key] = np.array(
                shapes[mod_key]).max(axis=0).tolist()

    def pad(self,array,max_dimensions):
        """Convenience function to pad objects. Fails if 
        `any(array.shape > max_dimensions)` or if 
        `len(array.shape) != len(max_dimensions)`.

        Args:
            array (np.array): a numpy array.
            max_dimensions (tuple): dimension of the padded array.
        
        Returns: 
            padded_array: an array with max_dimensions
        """
        
        array_sh = array.shape
        if len(array_sh) != len(max_dimensions):
            raise Exception(
                "array shape and max_dimensions need to be the same size.")
        if any([x>y for x,y in zip(array_sh,max_dimensions)]):
            raise Exception(
                "array shape cannot be bigger than max_dimensions.")
        
        padding = []
        for x,y in zip(max_dimensions,array_sh):
            t = int(x-y)
            a = t // 2
            b = t-a
            padding.append((a,b))
        
        padded_array = np.pad(array,padding)
        return padded_array

class SelectiveTransform(monai.transforms.Transform):
    def __init__(self,transform_dict: dict) -> monai.transforms.Transform:
        """Permits the application of different transforms to
        different elements of an input dict. Inherits from 
        `monai.transforms.Transform`

        Args:
            transform_dict (dict): dictionary with transforms (values)
            for each input key (keys)
        """
        self.transform_dict = transform_dict
    
    def __call__(self,X):
        """Applies transforms in `self.transform_dict` to the elements
        of `X` if the keys of `X` are in `self.transform_dict`.
        """
        output_dict = {}
        for k in X:
            if k in self.transform_dict:
                output_dict[k] = self.transform_dict[k](X[k])
        return output_dict

class MONAIDataset(monai.data.Dataset):
    def __init__(self,
                 path_dictionary: PathDictionary,
                 data_type: str,
                 padding: bool=False,
                 transforms: List=None,
                 orientation: str="RAS",
                 image_keys: List[str]=None) -> torch.utils.data.Dataset:
        """Joint dataset class for the generation of data from 
        a `path_dictionary` (details below). Works with MONAI

        Args:
            path_dictionary (dict): a dictionary containing paths
            to MRI scans.
            data_type: either "dicom" or "nifti". Everything else is assumed
            to be readable using `SimpleITK`.
            padding (bool,optional): pads the data to the inferred maximum 
            size. Defaults to False
            transforms (List, optional): List containing MONAI transforms. 
            Defaults to None
            orientation (str, optional): orientation to which the scans will
            be aligned. Defaults to "RAS"
            image_keys (List[str],optional): keys corresponding to images. 
            Defaults to None
            
        More particularly, the `path_dictionary` is structured as a
        dictionary where each key is an "MRI scan key" and the value 
        is a dictionary containing one path (folder) for each modality key. 
        In other words:
            
            path_dictionary
             |-MRI Scan 1
             | |-Modality 1: path_a
             | |-Modality 2: path_b
             |-MRI Scan 2:
               |-Modality 1: path_c
               |-Modality 2: path_d
               
        Or, more pythonically:
        `path_dictionary = {
            "MRI Scan 1": {"Modality 1": "path_a","Modality 2": "path_b"},
            "MRI Scan 2": {"Modality 1": "path_c","Modality 2": "path_d"}}`
        """
        
        self.path_dictionary = path_dictionary
        self.data_type = data_type
        self.padding = padding
        self.transforms = transforms
        self.orientation = orientation
        self.image_keys = image_keys
        if self.transforms is None:
            self.transforms = []

        self.keys = list(path_dictionary.keys())
        self.keys_original = self.keys.copy()
        self.length = len(self.keys)

        self.path_list = [self.path_dictionary[k] for k in self.keys]
        self.infer_mod_keys()
        self.automatic_id_exclusion()
        if self.data_type == "dicom":
            self.infer_max_dimensions_dicom()
        elif self.data_type == "nifti":
            self.infer_max_dimensions_nifti()
        else:
            self.infer_max_dimensions_sitk()
        self.setup_transform_ops()

        if self.image_keys is None:
            self.image_keys = self.mod_keys

        if self.orientation is not None:
            self.image_loader = monai.transforms.Compose([
                monai.transforms.LoadImaged(keys=self.image_keys),
                monai.transforms.AddChanneld(keys=self.image_keys),
                monai.transforms.Orientationd(
                    keys=self.image_keys,axcodes=self.orientation)])
        else:
            self.image_loader = monai.transforms.Compose([
                monai.transforms.LoadImaged(keys=self.image_keys),
                monai.transforms.AddChanneld(keys=self.image_keys)])

    def __len__(self):
        """Returns the number of MRI scans.

        Returns:
            length : Number of MRI scans
        """
        return self.length

    def __getitem__(self, index):
        """Returns the `self.keys[index]` MRI scan.

        Args:
            index (int or str): index of the MRI scan or key for the
            MRI scan in `self.path_dictionary`
            padding (bool, optional): pads each modality according to 
            `self.max_shapes`. Defaults to False.

        Returns:
            output_dict (dict): Dictionary containing `np.array` (value)
            for each modality (key)
        """
        if isinstance(index,str):
            k = index
        else:
            k = self.keys[index]
        
        path_dict = {kk:self.path_dictionary[k][kk] for kk in self.image_keys}
        X = self.image_loader(path_dict)
        if self.transform_ops is not None:
            X = self.transform_ops(X)
        #output_dict = {k:X[k] for k in self.image_keys}
        #for im_k in self.image_keys:
        #    X[im_k] = torch.Tensor(X[im_k])
        return X
    
    def update_keys(self,new_keys):
        """Updates MRI scan keys quickly. Allows the quick
        exchange between training/testing keys.

        Args:
            new_keys (list): list of keys (they need to be
            a subset of the original keys)
        """
        self.keys = []
        for k in new_keys:
            if k in self.keys_original:
                self.keys.append(k)
        self.length = len(self.keys)
    
    def infer_mod_keys(self):
        """Infers the keys for different modalities. If a modality
        is not present in at least 60% of all cases it gets excluded.
        """
        all_mod_keys = {}
        self.mod_keys = []
        self.excluded_mod_keys = []
        for k in self.keys:
            for kk in self.path_dictionary[k]:
                if kk not in all_mod_keys:
                    all_mod_keys[kk] = 1
                else:
                    all_mod_keys[kk] += 1
        for k in all_mod_keys:
            if (all_mod_keys[k] / self.length) > 0.6:
                self.mod_keys.append(k)
            else:
                self.excluded_mod_keys.append(k)
        self.n_modalities = len(self.mod_keys)
                
    def automatic_id_exclusion(self):
        """Excludes MRI scan keys based on modality key completeness.
        MRI scan keys are kept iif they have all the necessary 
        modalities.
        """
        good_keys = []
        bad_keys = []
        for k in self.keys:
            curr_keys = list(self.path_dictionary[k].keys())
            good = True
            for kk in self.mod_keys:
                if kk not in curr_keys:
                    good = False
            if good == True:
                good_keys.append(k)
            else:
                bad_keys.append(k)
        self.keys = good_keys
        self.excluded_keys = bad_keys
        self.length = len(self.keys)

    def get_random_images(self,n=1):
        """Generates a random set images from each MRI scan and 
        each modality.

        Args:
            n (int, optional): Number of images to be generated 
            for each MRI scan and modality. Defaults to 1.

        Yields:
            (pydicom array, mod_key): (a pydicom array, the 
            modality key)
        """
        for k in self.keys:
            for mod_key in self.mod_keys:
                mod_path = self.path_dictionary[k][mod_key]        
                all_dcm_paths = glob('{}/*dcm'.format(mod_path))
                random_dcm_paths = np.random.choice(all_dcm_paths,size=n)
                for dcm_path in random_dcm_paths:
                    yield pydicom.dcmread(dcm_path),mod_key
    
    def infer_max_dimensions_dicom(self):
        """Infers the maximum dimensions. Used for padding.
        """
        self.max_shapes = {}
        # get maximum height + width
        shapes = {mod_key:[] for mod_key in self.mod_keys}
        for dcm_image,mod_key in self.get_random_images():
            shapes[mod_key].append(dcm_image.pixel_array.shape)
        for mod_key in self.mod_keys:
            self.max_shapes[mod_key] = np.array(
                shapes[mod_key]).max(axis=0).tolist()
        
        # get maximum number of slices, combine with height + width
        n_slices = {mod_key:[] for mod_key in self.mod_keys}
        for k in self.keys:
            for mod_key in self.mod_keys:
                mod_path = self.path_dictionary[k][mod_key]
                n_slices[mod_key].append(len(glob('{}/*dcm'.format(mod_path))))
        for mod_key in n_slices:
            max_n_slices = np.max(n_slices[mod_key])
            self.max_shapes[mod_key].append(max_n_slices)
            # self.max_shapes[mod_key].reverse() # slice dim first

    def infer_max_dimensions_nifti(self):
        """Infers the maximum dimensions. Used for padding.
        """
        self.max_shapes = {}
        # get maximum height + width
        shapes = {mod_key:[] for mod_key in self.mod_keys}
        for k in self.keys:
            for mod_key in self.mod_keys:
                path = self.path_dictionary[k][mod_key]
                image = nibabel.load(path)
                shapes[mod_key].append(image.header.get_data_shape())

        for mod_key in shapes:
            self.max_shapes[mod_key] = np.array(
                shapes[mod_key]).max(axis=0).tolist()

    def infer_max_dimensions_nifti(self):
        """Infers the maximum dimensions. Used for padding.
        """
        self.max_shapes = {}
        # get maximum height + width
        shapes = {mod_key:[] for mod_key in self.mod_keys}
        for k in self.keys:
            for mod_key in self.mod_keys:
                path = self.path_dictionary[k][mod_key]
                image = nibabel.load(path)
                shapes[mod_key].append(image.header.get_data_shape())

        for mod_key in shapes:
            self.max_shapes[mod_key] = np.array(
                shapes[mod_key]).max(axis=0).tolist()

    def infer_max_dimensions_sitk(self):
        """Infers the maximum dimensions. Used for padding.
        """
        reader = sitk.ImageFileReader()
        self.max_shapes = {}
        # get maximum height + width
        shapes = {mod_key:[] for mod_key in self.mod_keys}
        for k in self.keys:
            for mod_key in self.mod_keys:
                path = self.path_dictionary[k][mod_key]
                reader.SetFileName(path)
                reader.LoadPrivateTagsOn()
                reader.ReadImageInformation()
                sh = reader.GetSize()
                shapes[mod_key].append(sh)

        for mod_key in shapes:
            self.max_shapes[mod_key] = np.array(
                shapes[mod_key]).max(axis=0).tolist()

    def setup_transform_ops(self):
        """Sets up MONAI transforms using self.transforms
        """
        transforms = []
        if self.padding == True:
            padding_op = SpatialPadDict(
                self.image_keys,self.max_shapes)
            transforms.append(padding_op)
        transforms.extend(self.transforms)
        if len(transforms) == 0:
            self.transform_ops = None
        else:
            self.transform_ops = monai.transforms.Compose(transforms)

class SpatialPadDict(monai.transforms.MapTransform,monai.transforms.InvertibleTransform):
    """Adaptation of monai.transforms.SpatialPadd that works with different pad sizes
    for different keys. Most of the class is copied from the original.
    """
    def __init__(
        self,
        keys: Union[Collection[Hashable],Hashable],
        spatial_size: Union[Sequence[int], int],
        allow_missing_keys: bool = False,
        **kwargs) -> None:
        """
        Args:
            keys: keys of the corresponding items to be transformed.
                See also: :py:class:`monai.transforms.compose.MapTransform`
            spatial_size: the spatial size of output data after padding, if a dimension of the input
                data size is bigger than the pad size, will not pad that dimension.
                If its components have non-positive values, the corresponding size of input image will be used.
                for example: if the spatial size of input data is [30, 30, 30] and `spatial_size=[32, 25, -1]`,
                the spatial size of output data will be [32, 30, 30].
            allow_missing_keys: don't raise exception if key is missing.
            kwargs: other arguments for the `np.pad` or `torch.pad` function.
                note that `np.pad` treats channel dimension as the first dimension.

        """
        super().__init__(keys, allow_missing_keys)
        if "mode" in kwargs:
            mode = kwargs["mode"]
            del kwargs["mode"]
        else:
            mode = monai.utils.NumpyPadMode.CONSTANT
        if "method" in kwargs:
            method = kwargs["method"]
            del kwargs["method"]
        else:
            method = monai.utils.Method.SYMMETRIC
        self.mode = monai.utils.ensure_tuple_rep(mode, len(self.keys))
        self.padder = {
            k:monai.transforms.SpatialPad(spatial_size[k], method,**kwargs)
            for k in self.keys}

    def __call__(self, data: Mapping[Hashable, torch.Tensor])->Dict[Hashable,torch.Tensor]:
        d = dict(data)
        for key, m in self.key_iterator(d, self.mode):
            self.push_transform(
                d, key,
                extra_info={"mode": m.value if isinstance(m, Enum) else m})
            d[key] = self.padder[key](d[key], mode=m)
        return d

    def inverse(self, data: Mapping[Hashable,torch.Tensor])->Dict[Hashable,torch.Tensor]:
        d = deepcopy(dict(data))
        for key in self.key_iterator(d):
            transform = self.get_most_recent_transform(d, key)
            # Create inverse transform
            orig_size = transform[monai.utils.enums.TraceKeys.ORIG_SIZE]
            if self.padder.method == monai.utils.Method.SYMMETRIC:
                current_size = d[key].shape[1:]
                roi_center = [floor(i / 2) if r % 2 == 0 else (i - 1) // 2
                              for r, i in zip(orig_size, current_size)]
            else:
                roi_center = [floor(r / 2) if r % 2 == 0 else (r - 1) // 2
                              for r in orig_size]

            inverse_transform = monai.transforms.SpatialCrop(roi_center, orig_size)
            # Apply inverse transform
            d[key] = inverse_transform(d[key])
            # Remove the applied transform
            self.pop_transform(d, key)

        return d

class TorchIODataset(torch.utils.data.Dataset):
    def __init__(self,
                 path_dictionary: PathDictionary,
                 data_type: str,
                 padding: bool=False,
                 transforms: List=None) -> torch.utils.data.Dataset:
        """Joint dataset class for the generation of data from 
        a `path_dictionary` (details below). Returns `torch` objects and
        uses TorchIO for the heavy lifting. 

        Args:
            path_dictionary (dict): a dictionary containing paths
            to MRI scans.
            data_type: either "dicom" or "nifti"
            padding (bool,optional): pads the data to the inferred maximum 
            size. Defaults to False
            transforms (dict, optional): dict containing lists of PyTorch transforms,
            one for each key. Defaults to None
            
        More particularly, the `path_dictionary` is structured as a
        dictionary where each key is an "MRI scan key" and the value 
        is a dictionary containing one path (folder) for each modality key. 
        In other words:
            
            path_dictionary
             |-MRI Scan 1
             | |-Modality 1: path_a
             | |-Modality 2: path_b
             |-MRI Scan 2:
               |-Modality 1: path_c
               |-Modality 2: path_d
               
        Or, more pythonically:
        `path_dictionary = {
            "MRI Scan 1": {"Modality 1": "path_a","Modality 2": "path_b"},
            "MRI Scan 2": {"Modality 1": "path_c","Modality 2": "path_d"}}`
        """
        
        self.path_dictionary = path_dictionary
        self.data_type = data_type
        self.padding = padding
        self.transforms = transforms
        if self.transforms is None:
            self.transforms = {}
        self.keys = list(path_dictionary.keys())
        self.keys_original = self.keys.copy()
        self.length = len(self.keys)
        
        self.infer_mod_keys()
        self.automatic_id_exclusion()
        if self.data_type == "dicom":
            self.infer_max_dimensions_dicom()
        else:
            self.infer_max_dimensions_nifti()
        self.setup_subject_dataset()
        
    def __len__(self):
        """Wrapper for the torchio.SubjectDataset method.

        Returns:
            length : Number of MRI scans
        """
        return len(self.subject_datasets[self.mod_keys[0]])

    def __getitem__(self, index):
        """Wrapper for the torchio.SubjectDataset method.

        Args:
            index (int): index of the MRI scan

        Returns:
            (Subject): a torchio.Subject object
        """
        
        output_dict = {}
        for mod_key in self.mod_keys:
            output_dict[mod_key] = self.subject_datasets[mod_key][index]
        return output_dict
                    
    def infer_mod_keys(self):
        """Infers the keys for different modalities. If a modality
        is not present in at least 60% of all cases it gets excluded.
        """
        all_mod_keys = {}
        self.mod_keys = []
        self.excluded_mod_keys = []
        for k in self.keys:
            for kk in self.path_dictionary[k]:
                if kk not in all_mod_keys:
                    all_mod_keys[kk] = 1
                else:
                    all_mod_keys[kk] += 1
        for k in all_mod_keys:
            if (all_mod_keys[k] / self.length) > 0.6:
                self.mod_keys.append(k)
            else:
                self.excluded_mod_keys.append(k)
        self.n_modalities = len(self.mod_keys)
                
    def automatic_id_exclusion(self):
        """Excludes MRI scan keys based on modality key completeness.
        MRI scan keys are kept iif they have all the necessary 
        modalities.
        """
        good_keys = []
        bad_keys = []
        for k in self.keys:
            curr_keys = list(self.path_dictionary[k].keys())
            good = True
            for kk in self.mod_keys:
                if kk not in curr_keys:
                    good = False
            if good == True:
                good_keys.append(k)
            else:
                bad_keys.append(k)
        self.keys = good_keys
        self.excluded_keys = bad_keys
        self.length = len(self.keys)

    def get_random_images(self,n=1):
        """Generates a random set images from each MRI scan and 
        each modality.

        Args:
            n (int, optional): Number of images to be generated 
            for each MRI scan and modality. Defaults to 1.

        Yields:
            (pydicom array, mod_key): (a pydicom array, the 
            modality key)
        """
        for k in self.keys:
            for mod_key in self.mod_keys:
                mod_path = self.path_dictionary[k][mod_key]        
                all_dcm_paths = glob('{}/*dcm'.format(mod_path))
                random_dcm_paths = np.random.choice(all_dcm_paths,size=n)
                for dcm_path in random_dcm_paths:
                    yield pydicom.dcmread(dcm_path),mod_key
    
    def infer_max_dimensions_dicom(self):
        """Infers the maximum dimensions. Used for padding.
        """
        self.max_shapes = {}
        # get maximum height + width
        shapes = {mod_key:[] for mod_key in self.mod_keys}
        for dcm_image,mod_key in self.get_random_images():
            shapes[mod_key].append(dcm_image.pixel_array.shape)
        for mod_key in self.mod_keys:
            self.max_shapes[mod_key] = np.array(
                shapes[mod_key]).max(axis=0).tolist()
        
        # get maximum number of slices, combine with height + width
        n_slices = {mod_key:[] for mod_key in self.mod_keys}
        for k in self.keys:
            for mod_key in self.mod_keys:
                mod_path = self.path_dictionary[k][mod_key]
                n_slices[mod_key].append(len(glob('{}/*dcm'.format(mod_path))))
        for mod_key in n_slices:
            max_n_slices = np.max(n_slices[mod_key])
            self.max_shapes[mod_key].append(max_n_slices)
            self.max_shapes[mod_key].reverse() # channels first

    def infer_max_dimensions_nifti(self):
        """Infers the maximum dimensions. Used for padding.
        """
        self.max_shapes = {}
        # get maximum height + width
        shapes = {mod_key:[] for mod_key in self.mod_keys}
        for k in self.keys:
            for mod_key in self.mod_keys:
                path = self.path_dictionary[k][mod_key]
                image = nibabel.load(path)
                shapes[mod_key].append(image.header.get_data_shape())

        for mod_key in shapes:
            self.max_shapes[mod_key] = np.array(
                shapes[mod_key]).max(axis=0).tolist()
            self.max_shapes[mod_key].reverse() # channels first

    def setup_subject_dataset(self):
        """Sets up Subjects (TorchIO class for data handling) and
        transforms.
        """
        self.subject_dict = {k:[] for k in self.mod_keys}
        self.subject_datasets = {k:[] for k in self.mod_keys}
        for mod_key in self.mod_keys:
            for k in self.keys:
                tmp = {
                    'image': torchio.ScalarImage(
                        self.path_dictionary[k][mod_key]),
                    'id': k}
                self.subject_dict[mod_key].append(
                    torchio.Subject(tmp))
            
            transforms = []
            if self.padding == True:
                transforms.append(torchio.transforms.CropOrPad(
                    self.max_shapes[mod_key],include="image"))
            if mod_key in self.transforms:
                transforms.extend(self.transforms[mod_key])
            transforms = torchio.Compose(transforms)
            self.subject_datasets[mod_key] = torchio.data.SubjectsDataset(
                self.subject_dict[mod_key],transform=transforms)

class Slicer(torch.utils.data.Dataset):
    def __init__(self,
                 torch_dataset: torch.utils.data.Dataset,
                 slice_dimension: int=1,
                 slice_type: str="uniform",
                 n_slices: int=16,
                 slice_size: int=1,
                 squeeze: bool=False,
                 transforms: torch.nn.Module=None,
                 ignore_edges: int=0) -> torch.utils.data.Dataset:
        """Data slicer for objects belonging to the PyTorch `Dataset`
        class. Takes a `Dataset` producing a set of N-d inputs and 
        outputs a similar structure where Tensors are transformed into
        lists of sliced Tensors along a given dimension. 

        Args:
            torch_dataset (torch.utils.data.Dataset): a PyTorch `Dataset`.
            The `__getitem__` method should return a `list`, `dict` or Tensor.
            slice_dimension (int, optional): dimension which will be sliced. 
            Defaults to 1.
            slice_type (str, optional): slice type. Can be one of "uniform" 
            (produces `n_slices` equally spaced slices), "random_uniform" 
            (randomly samples `n_slices` slices and each slice is equally
            likely to be sampled) and "random_normal" (same as 
            "random_uniform" but central slices are more likely to be 
            selected). Defaults to "uniform".
            n_slices (int, optional): Number of slices per Tensor. Defaults
            to 16.
            slice_size (int, optional): size of each slice. Defaults to 1.
            squeeze (bool, optional): whether to squeeze the slicing 
            dimension. Defaults to False.
            transforms (torch.nn.Module, optional): transforms to apply to
            individual slices. Defaults to None.
            ignore_edges (int, optional): Number of slices in the beginning
            or end along the `slice_dimension` that are to be excluded. 
            Defaults to 0.

        Returns:
            torch.utils.data.Dataset: _description_
        """
        self.torch_dataset = torch_dataset
        self.slice_dimension = slice_dimension
        self.slice_type = slice_type
        self.n_slices = n_slices
        self.slice_size = slice_size
        self.squeeze = squeeze
        self.transforms = transforms
        self.ignore_edges = ignore_edges

        self.normal_p = None
        
    def __len__(self) -> int:
        """Length of the Dataset.

        Returns:
            (int): length of `self.torch_dataset`
        """
        return len(self.torch_dataset)
    
    def __getitem__(self,i: int):
        """Returns an item from `self.torch_dataset` after slicing.

        Args:
            i (int or str): index of the item in `self.torch_dataset` or
            key

        Returns:
            output: returns an object similarly structured to the output
            of `self.torch_dataset.__getitem__(i)` but where each Tensor is
            split into `self.n_slices`
        """

        item = self.torch_dataset[i]
        if isinstance(item,tuple) == True:
            item,cl = item[0],item[1:]
        else:
            cl = None
        if isinstance(item,dict) == True:
            output = {}
            for k in item:
                output[k] = self.slice_tensor(item[k])
        elif isinstance(item,list) == True:
            output = []
            for tensor in item:
                output.append(self.slice_tensor(tensor))
        elif isinstance(item,torch.Tensor) == True:
            output = self.slice_tensor(item)
            
        if cl is not None:
            return (output,*cl)
        else:
            return output

    def slice_tensor(self,X: torch.Tensor) -> TensorList:
        """Slices a Tensor, producing a list containing `self.n_slices` slices
        of size `self.slice_size` along a given dimension
        (`self.slice_dimension`).

        Args:
            X (torch.Tensor): Torch tensor

        Returns:
            TensorList: list containing `self.n_slices` equally sized
            Tensors.
        """
        if self.ignore_edges > 0:
            sh = list(X.shape)
            M = sh[self.slice_dimension]
            d = [x for x in range(self.ignore_edges)]
            d.extend([-x for x in range(self.ignore_edges)])
            X = np.delete(X,d,self.slice_dimension)
        sh = list(X.shape)
        M = sh[self.slice_dimension]
        output = []
        if self.slice_type == "uniform":    
            idxs = np.linspace(M/(1+self.n_slices),M-self.slice_size,
                               endpoint=False,num=self.n_slices)
            idxs = [int(np.floor(idx)) for idx in idxs]
            idxs = [list(range(i,i+self.slice_size)) for i in idxs]
        
        if self.slice_type == "random_uniform":
            idxs = np.random.choice(
                M-self.slice_dimension,self.n_slices,replace=False)
            idxs = [list(range(i,i+self.slice_size)) for i in idxs]

        if self.slice_type == "random_normal":
            if self.normal_p is None:
                self.normal_p = norm(0,M//2).pdf(
                    np.arange(-M//2,M//2,num=M))
            elif self.normal_p.size != M:
                self.normal_p = norm(0,M//2).pdf(
                    np.arange(-M//2,M//2,num=M))

            idxs = np.random.choice(
                M-self.slice_dimension,self.n_slices,replace=False,
                p=self.normal_p)
            idxs = [list(range(i,i+self.slice_size)) for i in idxs]
        
        for idx in idxs:
            tmp = np.take(X,idx,self.slice_dimension)
            if self.transforms is not None:
                tmp = self.transforms(tmp)
            if self.squeeze == True:
                tmp = torch.squeeze(tmp,self.slice_dimension)
            output.append(tmp)

        return output

class Labels(torch.utils.data.Dataset):
    def __init__(self,
                 path: str,sep: str=',') -> torch.utils.data.Dataset:
        self.path = path
        self.sep = sep
        self.get_labels()
        self.get_counts()
        
    def __len__(self):
        return len(self.cd)
    
    def __getitem__(self,i):
        if isinstance(i,str):
            k = i
        else:
            k = self.keys[i]
        return self.one_hot_corr[self.cd[k]]
        
    def get_labels(self):
        with open(self.path,'r') as o:
            lines = o.readlines()[1:]
        
        lines = [x.strip().split(self.sep) for x in lines]
        self.cd = {k[0]:k[1] for k in lines}
        self.keys = list(self.cd.keys())
        self.values = sorted(np.unique(list(self.cd.values())))
        self.n_values = len(self.values)
        if self.n_values == 2:
            self.one_hot_corr = {
                self.values[i]:np.float32(i) for i in range(self.n_values)}
        else:
            self.one_hot_corr = {
                self.values[i]:make_one_hot(i,self.n_values)
                for i in range(self.n_values)}
        
        self.cd_original = self.cd.copy()
        self.keys_original = self.keys.copy()
    
    def get_counts(self):
        self.counts = {k:0 for k in self.values}
        for k in self.cd:
            self.counts[self.cd[k]] += 1
    
    def update_keys(self,new_keys):
        self.cd = {k:self.cd_original[k] for k in new_keys}
        self.keys = new_keys
        self.get_counts()

class ManyDatasets(torch.utils.data.Dataset):
    def __init__(self,
                 *datasets: torch.utils.data.Dataset) -> torch.utils.data.Dataset:
        self.datasets = datasets
        self.all_keys = [set(d.keys) for d in self.datasets]
        self.all_keys = list(set.intersection(*self.all_keys))
        self.length = len(self.all_keys)

    def __len__(self):
        return self.length
    
    def __getitem__(self,i):
        if isinstance(i,str):
            k = i
        else:
            k = self.all_keys[i]
        return tuple([d[k] for d in self.datasets])

class DatasetFilter(torch.utils.data.Dataset):
    def __init__(self,
                 dataset: torch.utils.data.Dataset,
                 key: str) -> torch.utils.data.Dataset:
        self.dataset = dataset
        self.key = key
        self.keys = self.dataset.keys
    
    def __len__(self):
        return len(self.dataset)
    
    def __getitem__(self,i):
        return self.dataset[i][self.key]